package dev.nahk.customerservice;

import dev.nahk.customerservice.config.GlobalConfig;
import dev.nahk.customerservice.entities.Customer;
import dev.nahk.customerservice.repository.CustomerRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import java.util.List;

@SpringBootApplication
@EnableConfigurationProperties(GlobalConfig.class)
public class CustomerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerServiceApplication.class, args);
	}

	@Bean
	CommandLineRunner commandLineRunner(CustomerRepository customerRepository) {
		return args -> {

			List<Customer> customerList = List.of(
					Customer.builder()
							.firstName("Harry")
							.lastName("Potter")
							.email("harry.potter@poudlard.co.uk")
							.build(),
					Customer.builder()
							.firstName("Hermione")
							.lastName("Granger")
							.email("hermione.granger@poudlard.co.uk")
							.build()
			);
			customerRepository.saveAll(customerList);
		};
	}

}
