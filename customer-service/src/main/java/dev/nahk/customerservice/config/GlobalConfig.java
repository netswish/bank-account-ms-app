package dev.nahk.customerservice.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties(prefix = "global.params")
public final class GlobalConfig {
    private int p1;
    private int p2;
}
