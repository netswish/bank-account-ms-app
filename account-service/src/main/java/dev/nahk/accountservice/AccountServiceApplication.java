package dev.nahk.accountservice;

import dev.nahk.accountservice.clients.CustomerRestClient;
import dev.nahk.accountservice.entities.BankAccount;
import dev.nahk.accountservice.enums.AccountType;
import dev.nahk.accountservice.repository.BankAccountRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;
import java.util.UUID;

@SpringBootApplication
@EnableFeignClients
public class AccountServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountServiceApplication.class, args);
	}

	@Bean
	CommandLineRunner commandLineRunner(BankAccountRepository accountRepository, CustomerRestClient customerRestClient) {
		return args -> {

			customerRestClient.allCustomers().forEach(customer -> {

				BankAccount bankAccount1 = BankAccount.builder()
						.accountId(UUID.randomUUID().toString())
						.currency("EUR")
						.balance(Math.random() * 8000)
						.createdAt(LocalDate.now())
						.type(AccountType.CURRENT_ACCOUNT)
						.customerId(customer.getId())
						.build();

				BankAccount bankAccount2 = BankAccount.builder()
						.accountId(UUID.randomUUID().toString())
						.currency("EUR")
						.balance(Math.random() * 12000)
						.createdAt(LocalDate.now())
						.type(AccountType.SAVING_ACCOUNT)
						.customerId(customer.getId())
						.build();

				accountRepository.save(bankAccount1);
				accountRepository.save(bankAccount2);
			});

//			BankAccount bankAccount1 = BankAccount.builder()
//					.accountId(UUID.randomUUID().toString())
//					.currency("EUR")
//					.balance(18000)
//					.createdAt(LocalDate.now())
//					.type(AccountType.CURRENT_ACCOUNT)
//					.customerId(Long.valueOf(1))
//					.build();
//			BankAccount bankAccount2 = BankAccount.builder()
//					.accountId(UUID.randomUUID().toString())
//					.currency("EUR")
//					.balance(2000)
//					.createdAt(LocalDate.now())
//					.type(AccountType.SAVING_ACCOUNT)
//					.customerId(Long.valueOf(2))
//					.build();
//
//			accountRepository.save(bankAccount1);
//			accountRepository.save(bankAccount2);
		};
	}

}
//	01:54:04 / 03:07:54